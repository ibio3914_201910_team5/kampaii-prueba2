angular.module('app.controllers', [])
  
.controller('usuarioCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
//var users = $firebaseArray(ref);
//$scope.users={
//    'usuario':data.usuario
//}

$scope.data={
    'Usuario': userUser,
    'MeanShots':userMeanS
}
}])
   
.controller('informaciNPersonalCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state) {
//var users = $firebaseArray(ref);
//$scope.users={
//    'usuario':data.usuario
//}

$scope.data={
    'Nombre': userUser,
    'Password': userPassword,
    'Email': userEmail,
    'Peso': userPeso,
    'Fecha': userBirth,
    'MeanShots':userMeanS
}
$scope.cerrarSesion=function(){
    userUser='';
    userName='';
    userPassword='';
    userEmail='';
    userBirth='';
    $state.go('login');
}
}])
   
.controller('domicilioCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('registroCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state) {
var contador=0;
$scope.sizes=[
    {
        'id': 'tequila',
        'label':'Tequila'
    },
    {
         'id': 'vodka',
        'label':'Vodka'
    },
    {
         'id': 'aguardiente',
        'label':'Aguardiente'
    },
    {
         'id': 'cerveza',
        'label':'Cerveza'
    }
    ];
    $scope.data={
        'size':$scope.sizes[0].id,
        'shot':false,
        'vaso':false
        
    }
    
    $scope.add=function(){
        if($scope.data.shot===true){
            contador++;
        }
        if($scope.data.vaso===true){
            contador++;
        }
        userMeanS=contador;
        $scope.data={
            'contador':userMeanS
        }
        userMeanS=$scope.data.contador;
        console.log($scope.data.contador);
        $state.go('tabsController.usuario',$scope.data);
        
    }
    //$scope.borrar=function(){
      //   $scope.data={
        //'size':$scope.sizes[0].id,
        //'shot':false,
        //'vaso':false
        
    //}
    //$state.go('tabsController.usuario',$scope.data);
    //}
}])
   
.controller('alcoholMetroCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
      
.controller('menuCtrl', ['$scope', '$stateParams', '$firebaseAuth', '$firebaseArray', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$firebaseAuth,$firebaseArray) {
    //if(prueba1!==null){
    $scope.cargar=function(){
        console.log("teodio");
    }
    
}])
   
.controller('loginCtrl', ['$scope', '$stateParams', '$state', '$firebaseAuth', '$firebaseArray', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state,$firebaseAuth,$firebaseArray) {//, $ionicSideMenuDelegate
    //$ionicSideMenuDelegate.canDragContent(false);
    //$scope.$on('$ionicView.leave', function () { $ionicSideMenuDelegate.canDragContent(false) });
    // this.MenuController.enable(false, 'Menu');
    var auth=$firebaseAuth();
    var autorizado=0;
    $scope.data={};
    
    $scope.login=function(){
        auth.$signInWithEmailAndPassword($scope.data.email,$scope.data.password).then($scope.loginSuceso).catch($scope.loginError); 
        autorizado=1;
        
    }
    $scope.loginSuceso=function(){
        
           if(autorizado==1)
        {
    var usersRef = firebase.database().ref("users");
    usersRef.child(firebase.auth().currentUser.uid).once("value").then(function(snapshot) 
            {
      var keyd= snapshot.key;
      var childkey= snapshot.child(keyd+"/nombre");
      //var usuarios=snapshot.child(keyd).val();
      //console.log(usuarios);
     // console.log("users/"+keyd);
      var valor="users/"+keyd;
      var ref = firebase.database().ref(valor);
ref.once("value")
  .then(function(snapshot) {
    var name = snapshot.child("nombre").val();
    var usuario = snapshot.child("usuario").val();
    var peso= snapshot.child("peso").val();
    var email= snapshot.child("email").val();
    var password= snapshot.child("password").val();
    var fecha = snapshot.child("fecha").val();
    var meanshot= snapshot.child("minshot").val();
    //console.log("nombre:"+name+"\n"+"Usuario:"+usuario+"\n"+"Peso:"+peso+"\n"+"Email:"+email);
    //console.log(prueba1);
    userUser=usuario;
    userName=name;
    userPassword=password;
    userEmail=email; 
    userPeso=peso;
    userBirth=fecha;
    userMeanS=meanshot;
   console.log(userEmail+"espacio" +email);
  });
            });
            
        }
        
        clear();
        $state.go('advertencia');
    }
    
    $scope.loginError=function(error){
        console.log(error);
    
        
    }
    //aquí
     const clear = () => {
         $scope.data.email.value = '';
         $scope.data.password.value = '';
     };
}])
   
.controller('signupCtrl', ['$scope', '$stateParams', '$firebaseArray', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $firebaseArray,$state) {
var auth=firebase.auth();
var ref= firebase.database().ref('users');
var users = $firebaseArray(ref);

$scope.data ={};

$scope.signupo = function(){
    
    console.log($scope.data)
    
    auth.createUserWithEmailAndPassword($scope.data.email,$scope.data.password).then($scope.signupSuceso);
}

$scope.signupSuceso=function(){
    

    var user = {
        nombre: $scope.data.nombre,
        peso: $scope.data.peso,
        email: $scope.data.email,
        usuario: $scope.data.usuario,
        password: $scope.data.password,
        fecha: $scope.data.fecha + "",
        minshot: 0,
        minspending:0 // guardar como un vector? Graficar? 
    }
    
    ref.child(firebase.auth().currentUser.uid).set(user);
   
    
    
  
    $state.go('login');
}
}
])
   
.controller('advertenciaCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$state) {
   
 

}])
 